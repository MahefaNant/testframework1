package exception;

public class UrlNotSupportedException extends Exception{
    public UrlNotSupportedException() {
        super("url not supported");
    }
}
