package com.example.project;

import annotation.UrlAnnotation;
import calcul.Calcul;
import exception.UrlNotSupportedException;
import function.Utilitaire;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

@WebServlet(name = "FrontServlet", value = "*.do")
public class FrontServlet extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    protected void processRequest() throws IOException , ServletException{
        PrintWriter out = response.getWriter();
        String allUrl = request.getRequestURI();
        String indice = new Utilitaire().retrieveUrlFromRawurl(allUrl);
        out.println("INDICE FROM URL = " + " { " + indice + " }");
        HashMap<Integer, Method> ms = new Utilitaire().allFunctionControllers(this);
        Method M = null;
        try {
            String methodName = new Utilitaire().methodToCall(ms,indice);
            try {
                if(methodName.equals("")) throw new UrlNotSupportedException();
                M = getClass().getDeclaredMethod(methodName);
                M.invoke(this);
            } catch (UrlNotSupportedException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @UrlAnnotation(url="rand")
    public void random() throws  IOException, ServletException {
        float a = Float.parseFloat(request.getParameter("a"));
        float b = Float.parseFloat(request.getParameter("b"));
        request.setAttribute("a", a);
        request.setAttribute("b", b);
        request.setAttribute("val", new Calcul().random(a,b));
        RequestDispatcher dispat = request.getRequestDispatcher("index.jsp");
        dispat.forward(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.request = request; this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        this.request = request; this.response = response;
        processRequest();
    }
}
