package function;

import annotation.UrlAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;

public class Utilitaire {
    public String retrieveUrlFromRawurl(String url) {
        String [] val = url.split("/");
        String [] val2 = val[2].split(".do");
        return val2[0];
    }

    public HashMap<Integer, Method> allFunctionControllers(Object cls) {
        Method[] MS = cls.getClass().getMethods();
        HashMap<Integer, Method>  result = new HashMap<>();
        int i=0;
        for(Method m : MS) {
            Annotation annotation = m.getAnnotation(UrlAnnotation.class);
            UrlAnnotation urlAnnotation = (UrlAnnotation) annotation;
            if(urlAnnotation!=null) result.put(i,m);
            i++;
        }
        return result;
    }

    public String methodToCall(HashMap<Integer, Method> ctrls, String indice) {
        for(int i=0;i<ctrls.size();i++) {
            Annotation annotation = ctrls.get(i).getAnnotation(UrlAnnotation.class);
            UrlAnnotation urlAnnotation = (UrlAnnotation) annotation;
            System.out.println(indice + " / // / "+ ((UrlAnnotation) annotation).url());
            if(urlAnnotation.url().equals(indice)) return ctrls.get(i).getName();
        }
        return "";
    }

}
